import "jest-extended";

import {
    daysBetween,
    afterIntervalTimes,
    recurringEvent,
} from "./Calculator";

import {
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

describe("daysBetween", () => {
    test("example 1", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(0);
    })
    test("example 2", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 30);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(1);
    })
    test("example 3", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,30);
        expect(daysBetween(start, end)).toEqual(-1);
    })
    test("example 4", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,3,31);
        expect(daysBetween(start, end)).toEqual(59);
    })
})

describe("afterIntervalTimes", () => {
    test("example 1", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval: Period = Period.parse("P1D");
        const multiplier: number = 3;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022,2,3));
    })
    test("example 2", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval: Period = Period.parse("P1D");
        const multiplier: number = 0;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022,1,31));
    })
    test("example 3", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval: Period = Period.parse("P1M");
        const multiplier: number = 1;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022,2,28));
    })
    test("example 4", () => {
        const start: LocalDate = LocalDate.of(2019, 1, 31);
        const interval: Period = Period.parse("P1Y1M");
        const multiplier: number = 1;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2020,2,29));
    })
});

describe("recurringEvent", () => {
    test("example 1", () => {
        const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 0, 0, 0);
        const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 23, 59, 0);
        const interval: Period = Period.parse("P1D");
        const timeOfDay: LocalTime = LocalTime.of(1,0,0);
        expect(recurringEvent(start,end,interval,timeOfDay)).toEqual([
            LocalDateTime.of(2022, 1, 1, 1, 0, 0),
            LocalDateTime.of(2022, 1, 2, 1, 0, 0),
            LocalDateTime.of(2022, 1, 3, 1, 0, 0),
            LocalDateTime.of(2022, 1, 4, 1, 0, 0)
        ]);
    })
    test("example 2", () => {
        const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 2, 0, 0);
        const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 23, 59, 0);
        const interval: Period = Period.parse("P1D");
        const timeOfDay: LocalTime = LocalTime.of(1,0,0);
        expect(recurringEvent(start,end,interval,timeOfDay)).toEqual([
            LocalDateTime.of(2022, 1, 2, 1, 0, 0),
            LocalDateTime.of(2022, 1, 3, 1, 0, 0),
            LocalDateTime.of(2022, 1, 4, 1, 0, 0)
        ]);
    })
    test("example 3", () => {
        const start: LocalDateTime = LocalDateTime.of(2022, 1, 1, 0, 0, 0);
        const end: LocalDateTime = LocalDateTime.of(2022, 1, 4, 0, 0, 0);
        const interval: Period = Period.parse("P1D");
        const timeOfDay: LocalTime = LocalTime.of(1,0,0);
        expect(recurringEvent(start,end,interval,timeOfDay)).toEqual([
            LocalDateTime.of(2022, 1, 1, 1, 0, 0),
            LocalDateTime.of(2022, 1, 2, 1, 0, 0),
            LocalDateTime.of(2022, 1, 3, 1, 0, 0)
        ]);
    })
    test("example 4", () => {
        const start: LocalDateTime = LocalDateTime.of(2022, 1, 31, 0, 0, 0);
        const end: LocalDateTime = LocalDateTime.of(2022, 5, 15, 0, 0, 0);
        const interval: Period = Period.parse("P1M");
        const timeOfDay: LocalTime = LocalTime.of(1,0,0);
        expect(recurringEvent(start,end,interval,timeOfDay)).toEqual([
            LocalDateTime.of(2022, 1, 31, 1, 0, 0),
            LocalDateTime.of(2022, 2, 28, 1, 0, 0),
            LocalDateTime.of(2022, 3, 28, 1, 0, 0),
            LocalDateTime.of(2022, 4, 28, 1, 0, 0)
        ]);
    })
});