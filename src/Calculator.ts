import {
    ChronoUnit,
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

export function daysBetween(
    start: LocalDate,
    end: LocalDate,
): number {
    const daysDifference = start.until(end, ChronoUnit.DAYS);
    return daysDifference;
}

export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number,
): LocalDate {
    return start.plus(interval.multipliedBy(multiplier));
}

export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    const startTime : LocalTime = start.toLocalTime();
    const endTime : LocalTime = end.toLocalTime();
    const endDate : LocalDate = end.toLocalDate();
    let cur : LocalDateTime = start;
    const ret : LocalDateTime[] = [];
    cur = cur.with(timeOfDay);
    if (! startTime.isBefore(timeOfDay))
        cur = cur.plus(interval);
    let curDate : LocalDate = cur.toLocalDate();
    while (curDate.isBefore(endDate)) {
        ret.push(cur);
        cur = cur.plus(interval);
        curDate = cur.toLocalDate();
    }
    if (endTime.isAfter(timeOfDay))
        ret.push(cur);
    return ret;
}